import discord
from discord.ext import commands
from datetime import datetime, timedelta
import csv

# As of my initial glance at the documentation, a prefix should be included, to prevent interference with the existing bot, I have selected '!'
bot = commands.Bot(command_prefix='!')

# Replace with bot token
bot_token = 'YOUR_BOT_TOKEN'

scores = {}

# Local CSV file containing the vote records.
VOTE_RECORDS_FILE = "vote_records.csv"

# Checks if user has voted in the past 24 hours.
def can_vote(user_id):
    if user_id not in vote_records:
        return True
    last_vote_time = datetime.fromisoformat(vote_records[user_id])
    return datetime.now() > last_vote_time + timedelta(hours=24)

# Nick update handler.
async def update_nickname(user, nickname):
    await user.edit(nick=nickname)

# File saving funcs
def save_vote_records():
    with open(VOTE_RECORDS_FILE, mode='w', newline='') as vote_file:
        writer = csv.writer(vote_file)
        writer.writerow(["user_id", "last_vote_time"])
        for user_id, last_vote_time in vote_records.items():
            writer.writerow([user_id, last_vote_time])

def load_vote_records():
    try:
        with open(VOTE_RECORDS_FILE, mode='r') as vote_file:
            reader = csv.DictReader(vote_file)
            for row in reader:
                user_id = int(row["user_id"])
                last_vote_time = row["last_vote_time"]
                vote_records[user_id] = last_vote_time
    except FileNotFoundError:
        pass

vote_records = {}
load_vote_records()

@bot.command()
async def vote(ctx, user: discord.Member, vote_type: str):
    if not can_vote(ctx.author.id):
        await ctx.send("You have already voted within the last 24 hours!")
        return

    if user.id == ctx.author.id:
        await ctx.send("You can't vote for yourself!")
        return

    if vote_type == 'positive':
        score_change = 1
        emoji = "👍"
    elif vote_type == 'negative':
        score_change = -1
        emoji = "👎"
    else:
        await ctx.send("Invalid vote type!")
        return

    if user.id not in vote_records:
        vote_records[user.id] = datetime.now().isoformat()
    scores[user.id] = scores.get(user.id, 0) + score_change
    vote_count = scores[user.id]
    await ctx.send(f"{ctx.author.mention} voted {emoji} for {user.mention} ({vote_count} votes).")

# Emoticons can be swapped.
    if vote_count >= 100:
        await update_nickname(user, f"💯 {user.name}")
    elif vote_count >= 10:
        await update_nickname(user, f"🔟 {user.name}")
    elif vote_count >= 1:
        await update_nickname(user, f"1️⃣ {user.name}")

    vote_records[ctx.author.id] = datetime.now().isoformat()
    save_vote_records()

bot.run(bot_token)
