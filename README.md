# golden-member-bot


This bot is a prototype bot intended for voting on live call ins.

The workflow I would imagine would work best, is that when people are permitted to call in, they will have their username pop up in a "VIP channel" and viewers are able to vote in the affirmative or negative based on the call. This channel would be exclusively for voting, and we could establish a 15 minute voting window where people can vote on it.

To prevent self boosting, we should include a way to prevent people to self vote, and in the back of my mind, we should also prevent other people who are calling in from voting - as it could be used maliciously to sway opinions on people submitting. I believe this should be a cosmetic addition only, but there's no doubt that it would sway the deciding team's decision to see a frequent caller.

VIP lounge is already full of "1 roll caller" or "20 roll caller" ranking them, and this popped into mind as a project.

As of now, it will store the votes into a CSV file called 'vote_records.csv' but this can easily be altered to support most database schemas depending on what your backend looks like.


